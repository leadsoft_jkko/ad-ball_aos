package com.example.adball.view.intro;

import android.content.Intent;
import android.os.Bundle;

import androidx.databinding.DataBindingUtil;

import com.example.adball.R;
import com.example.adball.base.ActivityBase;
import com.example.adball.manager.PreferenceManager;
import com.example.adball.databinding.ActivityIntroBinding;
import com.example.adball.view.login.ActivityLogin;
import com.example.adball.view.main.ActivityMain;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;

public class ActivityIntro extends ActivityBase {

    private ActivityIntroBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        this.binding = DataBindingUtil.setContentView(this, R.layout.activity_intro);

        versionChecked();
    }

    private void versionChecked() {
        Observable.timer(3000, TimeUnit.MILLISECONDS)
                .subscribe(timer ->{
                    Class activity;
                    if (PreferenceManager.getLoginState(this))
                        activity = ActivityMain.class;
                    else
                        activity = ActivityLogin.class;
                    startActivity(new Intent(ActivityIntro.this, activity));
                });
    }
}
