package com.example.adball.view.main;

import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;

import com.example.adball.R;
import com.example.adball.base.ActivityBase;
import com.example.adball.manager.PreferenceManager;
import com.example.adball.databinding.ActivityMainBinding;
import com.example.adball.view.settings.ActivitySettings;

public class ActivityMain extends ActivityBase {

    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        PreferenceManager.setLoginState(this, true);

        this.binding.button.setOnClickListener(v->
            startActivity(new Intent(ActivityMain.this, ActivitySettings.class)));
    }
}
