package com.example.adball.view.settings;

import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;

import com.example.adball.R;
import com.example.adball.base.ActivityBase;
import com.example.adball.manager.Const;
import com.example.adball.databinding.ActivitySettingsBinding;
import com.example.adball.view.login.ActivityLogin;

public class ActivitySettings extends ActivityBase {

    private ActivitySettingsBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        this.binding = DataBindingUtil.setContentView(this, R.layout.activity_settings);

        this.binding.settingsBtnLogout.setOnClickListener(v-> new AlertDialog.Builder(this).setMessage(getString(R.string.settings_btn_logout_alert))
                .setPositiveButton(getString(R.string.answer_confirm), (dialog, which) -> {
                    startActivityForResult(new Intent(ActivitySettings.this, ActivityLogin.class), Const.ACTIVITY_CALL_LOGIN);
                    dialog.dismiss();
                })
                .setNegativeButton(getString(R.string.answer_cancel), (dialog, which) -> dialog.dismiss())
                .create().show());
    }
}
