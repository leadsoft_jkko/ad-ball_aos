package com.example.adball.view.login;

import androidx.databinding.DataBindingUtil;

import android.os.Bundle;

import com.example.adball.R;
import com.example.adball.base.ActivityBase;
import com.example.adball.manager.PreferenceManager;
import com.example.adball.databinding.ActivityLoginBinding;
import com.example.adball.view.main.ActivityMain;

public class ActivityLogin extends ActivityBase {

    private ActivityLoginBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        this.binding = DataBindingUtil.setContentView(this, R.layout.activity_login);

        PreferenceManager.setFirstTimeAccess(this, false);
        PreferenceManager.setLoginState(this, false);

        this.binding.loginButton.setOnClickListener(v->
                startActivity(this, ActivityMain.class));
    }


}
