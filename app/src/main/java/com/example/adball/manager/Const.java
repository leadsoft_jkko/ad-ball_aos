package com.example.adball.manager;

public class Const {

    public static final String PREFERENCE_NAME = "preference name";

    public static final String PREFERENCE_FIRST_TIME_ACCESS = "preference fist time access";

    public static final String PREFERENCE_LOGIN_STATE = "preference login state";


    // Activity 이동 시 RequestCode 값
    // 1000 단위 :
    // 2000 단위 :
    // 3000 단위 :
    // 4000 단위 :
    // 5000 단위 :
    public static final int ACTIVITY_CALL_LOGIN = 10000;
}
