package com.example.adball.manager;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

public class PreferenceManager {

    public static boolean getFistTimeAccess(Context context){
        SharedPreferences pref = context.getSharedPreferences(Const.PREFERENCE_NAME, Activity.MODE_PRIVATE);
        return pref.getBoolean(Const.PREFERENCE_FIRST_TIME_ACCESS, true);
    }

    public static void setFirstTimeAccess(Context context, boolean value){
        SharedPreferences pref = context.getSharedPreferences(Const.PREFERENCE_NAME, Activity.MODE_PRIVATE);
        SharedPreferences.Editor e = pref.edit();
        e.putBoolean(Const.PREFERENCE_FIRST_TIME_ACCESS, value);
        e.commit(); // FIXME :: .apply() 적용을 고려
    }

    public static boolean getLoginState(Context context){
        SharedPreferences pref = context.getSharedPreferences(Const.PREFERENCE_NAME, Activity.MODE_PRIVATE);
        return pref.getBoolean(Const.PREFERENCE_LOGIN_STATE, false);
    }

    public static void setLoginState(Context context, boolean value){
        SharedPreferences pref = context.getSharedPreferences(Const.PREFERENCE_NAME, Activity.MODE_PRIVATE);
        SharedPreferences.Editor e = pref.edit();
        e.putBoolean(Const.PREFERENCE_LOGIN_STATE, value);
        e.commit();
    }


}
