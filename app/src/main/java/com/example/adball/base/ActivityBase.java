package com.example.adball.base;

import android.content.Context;
import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;

public class ActivityBase extends AppCompatActivity {


    public void startActivity(Context nowActivity, Class nextActivity){
        startActivity(new Intent(nowActivity, nextActivity));
    }
}
